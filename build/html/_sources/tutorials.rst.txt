Tutorials
=========

Videos
======


Como utilizar a função de acesso excepcional - pt-BR
----------------------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/zHUAgHQQuV0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Como realizar o tuning do sistema - pt-BR
-----------------------------------------

.. raw:: html
   
   <iframe width="560" height="315" src="https://www.youtube.com/embed/YdtNhwnBBNg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Configuração de rede - pt-BR
----------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/-RTg36MTNtU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Como realizar transferência de arquivos Via SFTP Winscp - pt-BR
---------------------------------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/dvbfZ-FQgg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Como criar um novo usuário administrador local - pt-BR
------------------------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/1ZwH8y4rrnI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Como realizar a integração SMTP - pt-BR
---------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/C_jrhafQWMI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>