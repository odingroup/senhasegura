Welcome to senhasegura's documentation!
=======================================

We strive to ensure the sovereignty of companies over actions and privileged information. To this end, we work against data theft through traceability of administrator actions on networks, servers, databases and a multitude of devices. 
In addition, we pursue compliance with auditing requirements and the most demanding standards, including PCI DSS, Sarbanes-Oxley, ISO 27001 and HIPAA.

What we believe
---------------

For senhasegura, protection, access, and confidentiality of privileged information are fundamental rights of any organization and society as a whole.

Our mission
-----------

To help organizations build sovereignty and security over access and privileged information.

Our Vision
----------

To be the best privileged access management solution in the global market.

Why senhasegura?
----------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/fqmWtC8YAyc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   getstarted
   tutorials
   docs
   API